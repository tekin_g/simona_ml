
import tensorflow as tf

from sklearn.model_selection import KFold
from tensorflow.keras.initializers import RandomNormal
from tensorflow.keras.initializers import Zeros

########################### Defs ###########################
# Dataset needs to be splitted into training and validation, the training set is used for training, the validation set
# is just used afterwards to check the performance for new data
def split_dataSet(input1 ,output1 ,n_splits):
    '''function to split it into training and validation
     '''
    input1.reset_index(drop=True, inplace=True)
    output1.reset_index(drop=True, inplace=True)

    # kf = KFold(n_splits=5, random_state=76, shuffle=True)
    kf = KFold(n_splits=n_splits, random_state=76, shuffle=True)

    train_ind, val_ind = next(kf.split(input1))

    input_train = input1.values[train_ind]
    input_val = input1.values[val_ind]
    output_train = output1.values[train_ind]
    output_val = output1.values[val_ind]

    return input_train, input_val, output_train, output_val


def scaling(df ,df_min ,df_max ,scale_min ,scale_max):
    df_std = (df -df_min) / ( 0 +df_max -df_min)
    df_scaled = df_std* ( 0 +scale_max - scale_min ) +scale_min
    return df_scaled

def scaling_reverse(df ,df_min ,df_max ,scale_min ,scale_max):
    df_scaled_reverse = (df -scale_min ) /( 0 +scale_max -scale_min ) *( 0 +df_max -df_min ) +df_min
    return df_scaled_reverse


def custom_loss_function(y_true, y_pred):

    '''A = plt.hist(y_true - y_pred,bins=20)
    xx = A[0]
    yy = A[1]
    plt.plot(xx,yy)'''
    w1 = 0.5
    w2 = 0.5
    loss_custom = w1 *tf.abs(y_true - y_pred) + w2* tf.abs(tf.reduce_mean(y_true - y_pred))
    return tf.reduce_mean(loss_custom, axis=-1)


def build_NN(dimX, dimY, config,normalization,uncompiled_model=False,name = None):
    # build the surrogate model

    # define the lossfunction you use for training
    # loss = tf.keras.losses.MeanSquaredError()
    loss = tf.keras.losses.mse # mse mean_absolute_error

    # define the metrics to decide on the performance of the NN
    # metrics = [tf.keras.losses.mse, tf.keras.losses.mean_absolute_percentage_error, tf.keras.losses.mean_absolute_error]
    metrics = [tf.keras.losses.mse] # [tf.keras.losses.mse]      mse mean_absolute_error
    first_batch_norm = tf.keras.layers.BatchNormalization()
    hidden_layers = [first_batch_norm]

    for i in range(config['depth']-1): # Generate N-1 layers with batch normalization or dropout


        layer = tf.keras.layers.Dense(units = config['width'], activation =config['activation']
                                      ,activity_regularizer=tf.keras.regularizers.l2
                                          (config['l2 regularization penalty'])
                                      ,kernel_initializer=RandomNormal(stddev=0.01), bias_initializer=Zeros())

        hidden_layers.append(layer)

        batchnorm = tf.keras.layers.BatchNormalization()
        hidden_layers.append(batchnorm)
        dropout = tf.keras.layers.Dropout(config['dropout'])
        hidden_layers.append(dropout)
    # the last layer gives you the final output
    output_layer = tf.keras.layers.Dense(units = dimY, activation = config['activation'] )
    # here you plug all the layers together
    layers = hidden_layers + [output_layer]
    if config['normalization']:
        layers = [normalization] + hidden_layers + [output_layer]
    if name is not None:
        model = tf.keras.models.Sequential(layers,name=name)
    else:
        model = tf.keras.models.Sequential(layers)
    if uncompiled_model:
        return model

    # choose an optimization algorithm
    optimizer = tf.keras.optimizers.Adam(learning_rate=config['learning_rate'])

    # here you build the model
    model.compile(
        loss=loss, # loss, custom_loss_function,
        optimizer=optimizer,
        metrics=metrics
    )


    print(model.summary())

    return model


class RSquaredSeparated(tf.keras.losses.Loss):
    '''
    Calculates the R^2 value for each predicted quantity separately.

    For more details, see:
    https://www.analyticsvidhya.com/blog/2020/07/difference-between-r-squared-and-adjusted-r-squared/
    '''
    def __init__(self):
        super().__init__(name='r2')

    def call(self, y_true, y_pred):
        mean_true = tf.math.reduce_mean(y_true, axis=0)

        total_sum_of_squares = tf.math.reduce_sum(tf.math.squared_difference(y_true, mean_true),
                                                  axis=0)
        residual_sum_of_squares = tf.math.reduce_sum(tf.math.squared_difference(y_true, y_pred),
                                                     axis=0)
        r2 = 1. - residual_sum_of_squares / total_sum_of_squares

        return r2


#############################################################
