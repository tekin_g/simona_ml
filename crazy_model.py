import tensorflow as tf
import numpy as np
from keras import Input
from keras.src.regularizers import regularizers
from tensorflow.keras.layers import Layer, Dense, Dropout, BatchNormalization
from tensorflow.keras.models import Model
def calculate_shapes(input_shape, latent_dim, num_layers):
    if num_layers == 1:
        return [latent_dim]
    shapes = np.geomspace(input_shape, latent_dim, num_layers).astype(int)
    return shapes
class Autoencoder(Model):
    def __init__(self, config, **kwargs):
        super(Autoencoder, self).__init__(**kwargs)
        self.input_dim = config['input_dim']
        self.latent_dim = config['latent_dim']
        self.depth = config['encoder_depth']
        self.encoder_structure = calculate_shapes(self.input_dim, self.latent_dim, self.depth)
        self.decoder_structure = self.encoder_structure[-1::-1]

        if len (self.encoder_structure) == 1: # fix one layer bug
            self.decoder_structure = [self.input_dim]

        encoder_layers = []
        encoder_layers.append(Input((self.input_dim,)))
        encoder_layers.append(BatchNormalization(name='batch_e1'))
        encoder_layers.append(Dense(self.encoder_structure[0], activation=config['activation'], kernel_regularizer=regularizers.L2(config['l2_reg'])))
        for i in self.encoder_structure[1:]:
            encoder_layers.append(BatchNormalization(name=f'batch_e{i}'))
            encoder_layers.append(Dropout(config['dropout']))
            encoder_layers.append(Dense(i, activation=config['activation'], kernel_regularizer=regularizers.L2(config['l2_reg'])))
            # encoder_layers.append(layers.Dense(i, activation='relu'))

        self.encoder = tf.keras.Sequential(encoder_layers)

        decoder_layers = []
        decoder_layers.append(BatchNormalization(name='batch_d1'))
        decoder_layers.append(Dense(self.decoder_structure[0], activation=config['activation'], kernel_regularizer=regularizers.L2(config['l2_reg'])))
        for i in self.decoder_structure[1:]:
            decoder_layers.append(BatchNormalization(name=f'batch_d{i}'))
            decoder_layers.append(Dropout(config['dropout']))
            decoder_layers.append(Dense(i, activation=config['activation'],kernel_regularizer=regularizers.L2(config['l2_reg'])))

        self.decoder = tf.keras.Sequential(decoder_layers)
    def call(self, x,training=False):
        encoded = self.encoder(x,training=training)
        decoded = self.decoder(encoded,training=training)
        return encoded,decoded

    def get_config(self):
        return dict(config=self.config)


class CrazyModel(tf.keras.Model):
    def __init__(self,config,**kwargs):
        super(CrazyModel, self).__init__(**kwargs)
        self.autoencoder = Autoencoder(config,**kwargs)
        self.config = config
        width = config['network_width']
        layers = []
        layers.append(BatchNormalization(name='batch_f'))
        layers.append(Dense(width,activation=config['activation'],kernel_regularizer=regularizers.L2(config['l2_reg'])))
        for i in range(config['network_depth']-1):
            layers.append(BatchNormalization(name=f'batch_f{i}'))
            layers.append(Dropout(config['dropout']))
            layers.append(Dense(width,activation=config['activation'],kernel_regularizer=regularizers.L2(config['l2_reg'])))
        self.final = tf.keras.Sequential(layers)
    def call(self, x, training=False):
        encoded,decoded = self.autoencoder(x,training=training)
        return decoded, self.final(encoded,training=training)

    def get_config(self):
        return dict(config=self.config)





class CrazyModelCorr(tf.keras.Model):
    def __init__(self,config,**kwargs):
        super(CrazyModelCorr, self).__init__(**kwargs)
        self.autoencoder = Autoencoder(config,**kwargs)
        self.config = config
        width = config['network_width']
        layers = []
        layers.append(BatchNormalization(name='batch_f'))
        layers.append(Dense(width,activation=config['activation'],kernel_regularizer=regularizers.L2(config['l2_reg'])))
        for i in range(config['network_depth']-1):
            layers.append(BatchNormalization(name=f'batch_f{i}'))
            layers.append(Dropout(config['dropout']))
            layers.append(Dense(width,activation=config['activation'],kernel_regularizer=regularizers.L2(config['l2_reg'])))
        self.final = tf.keras.Sequential(layers)
    def call(self, x, training=False):
        encoded,decoded = self.autoencoder(x[0],training=training)
        input = tf.concat([encoded,x[1]],axis=-1)
        return decoded, self.final(input,training=training)

    def get_config(self):
        return dict(config=self.config)

